# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended to check this file into your version control system.

ActiveRecord::Schema.define(:version => 20130523205714) do

  create_table "equipment", :force => true do |t|
    t.string   "name",       :null => false
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "exerciseequipments", :force => true do |t|
    t.integer  "exercise_id",  :null => false
    t.integer  "equipment_id", :null => false
    t.datetime "created_at",   :null => false
    t.datetime "updated_at",   :null => false
  end

  add_index "exerciseequipments", ["equipment_id"], :name => "index_exerciseequipments_on_equipment_id"
  add_index "exerciseequipments", ["exercise_id"], :name => "index_exerciseequipments_on_exercise_id"

  create_table "exercisemusclegroups", :force => true do |t|
    t.integer  "exercise_id"
    t.integer  "muscle_group_id"
    t.datetime "created_at",      :null => false
    t.datetime "updated_at",      :null => false
  end

  add_index "exercisemusclegroups", ["exercise_id"], :name => "index_exercisemusclegroups_on_exercise_id"
  add_index "exercisemusclegroups", ["muscle_group_id"], :name => "index_exercisemusclegroups_on_muscle_group_id"

  create_table "exercises", :force => true do |t|
    t.string   "name",             :null => false
    t.datetime "created_at",       :null => false
    t.datetime "updated_at",       :null => false
    t.string   "circuit_reps"
    t.string   "strength_reps"
    t.string   "exercise_type"
    t.string   "hypertrophy_reps"
    t.text     "instructions"
  end

  create_table "gymequipments", :force => true do |t|
    t.integer  "gym_id"
    t.integer  "equipment_id"
    t.datetime "created_at",   :null => false
    t.datetime "updated_at",   :null => false
  end

  add_index "gymequipments", ["equipment_id"], :name => "index_gymequipments_on_equipment_id"
  add_index "gymequipments", ["gym_id"], :name => "index_gymequipments_on_gym_id"

  create_table "gyms", :force => true do |t|
    t.string   "name"
    t.string   "street_address"
    t.string   "city"
    t.string   "state"
    t.string   "website"
    t.string   "hours"
    t.datetime "created_at",     :null => false
    t.datetime "updated_at",     :null => false
    t.float    "latitude"
    t.float    "longitude"
    t.string   "phone_number"
    t.string   "zip"
  end

  create_table "messages", :force => true do |t|
    t.string   "name"
    t.string   "email"
    t.string   "subject"
    t.text     "body"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "muscle_groups", :force => true do |t|
    t.string   "name"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "simple_captcha_data", :force => true do |t|
    t.string   "key",        :limit => 40
    t.string   "value",      :limit => 6
    t.datetime "created_at",               :null => false
    t.datetime "updated_at",               :null => false
  end

  add_index "simple_captcha_data", ["key"], :name => "idx_key"

  create_table "users", :force => true do |t|
    t.string   "name"
    t.string   "email"
    t.datetime "created_at",                                :null => false
    t.datetime "updated_at",                                :null => false
    t.string   "password_digest"
    t.string   "remember_token"
    t.boolean  "admin",                  :default => false
    t.string   "password_reset_token"
    t.datetime "password_reset_sent_at"
    t.float    "longitude"
    t.float    "latitude"
    t.string   "ip_address"
  end

  add_index "users", ["email"], :name => "index_users_on_email", :unique => true
  add_index "users", ["remember_token"], :name => "index_users_on_remember_token"

  create_table "workoutequipments", :force => true do |t|
    t.integer  "workout_id",   :null => false
    t.integer  "equipment_id", :null => false
    t.datetime "created_at",   :null => false
    t.datetime "updated_at",   :null => false
  end

  add_index "workoutequipments", ["equipment_id"], :name => "index_workoutequipments_on_equipment_id"
  add_index "workoutequipments", ["workout_id"], :name => "index_workoutequipments_on_workout_id"

  create_table "workoutexercises", :force => true do |t|
    t.integer  "workout_id",      :null => false
    t.integer  "exercise_id",     :null => false
    t.datetime "created_at",      :null => false
    t.datetime "updated_at",      :null => false
    t.integer  "position"
    t.integer  "exercise_weight"
  end

  add_index "workoutexercises", ["exercise_id"], :name => "index_workoutexercises_on_exercise_id"
  add_index "workoutexercises", ["workout_id"], :name => "index_workoutexercises_on_workout_id"

  create_table "workoutmusclegroups", :force => true do |t|
    t.integer  "workout_id"
    t.integer  "muscle_group_id"
    t.datetime "created_at",      :null => false
    t.datetime "updated_at",      :null => false
  end

  add_index "workoutmusclegroups", ["muscle_group_id"], :name => "index_workoutmusclegroups_on_muscle_group_id"
  add_index "workoutmusclegroups", ["workout_id"], :name => "index_workoutmusclegroups_on_workout_id"

  create_table "workouts", :force => true do |t|
    t.string   "name"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
    t.integer  "user_id"
    t.string   "category"
  end

  add_index "workouts", ["user_id", "created_at"], :name => "index_workouts_on_user_id_and_created_at"

end
