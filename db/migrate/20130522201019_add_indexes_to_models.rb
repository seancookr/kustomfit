class AddIndexesToModels < ActiveRecord::Migration
  def change
    add_index :exerciseequipments,   :exercise_id
    add_index :exerciseequipments,   :equipment_id
    add_index :exercisemusclegroups, :exercise_id
    add_index :exercisemusclegroups, :muscle_group_id
    add_index :gymequipments,        :gym_id
    add_index :gymequipments,        :equipment_id
    add_index :workoutequipments,    :workout_id
    add_index :workoutequipments,    :equipment_id
    add_index :workoutexercises,     :workout_id
    add_index :workoutexercises,     :exercise_id
    add_index :workoutmusclegroups,  :workout_id
    add_index :workoutmusclegroups,  :muscle_group_id
  end
end
