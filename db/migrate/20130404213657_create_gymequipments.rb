class CreateGymequipments < ActiveRecord::Migration
  def change
    create_table :gymequipments do |t|
      t.integer :gym_id
      t.integer :equipment_id

      t.timestamps
    end
  end
end
