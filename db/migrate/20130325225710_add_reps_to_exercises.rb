class AddRepsToExercises < ActiveRecord::Migration
  def change
    add_column :exercises, :circuit_reps, :string
    add_column :exercises, :strength_reps, :string
  end
end
