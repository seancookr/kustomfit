class CreateWorkoutexercises < ActiveRecord::Migration
  def change
    create_table :workoutexercises do |t|
      t.integer :workout_id, :null => false
      t.integer :exercise_id, :null => false

      t.timestamps
    end
  end
end
