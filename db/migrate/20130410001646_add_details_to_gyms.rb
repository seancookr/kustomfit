class AddDetailsToGyms < ActiveRecord::Migration
  def change
    add_column :gyms, :phone_number, :string
    add_column :gyms, :zip, :string
  end
end
