class CreateGyms < ActiveRecord::Migration
  def self.up
    create_table :gyms do |t|
      t.string :name
      t.string :street_address
      t.string :city
      t.string :state
      t.integer :zip
      t.integer :phone_number
      t.string :website
      t.string :hours
      t.timestamps
    end
  end

  def self.down
    drop_table :gyms
  end
end
