class AddUseridToWorkouts < ActiveRecord::Migration
  def change
    add_column :workouts, :user_id, :integer
    add_index :workouts, [:user_id, :created_at]
  end
end
