class CreateWorkoutequipments < ActiveRecord::Migration
  def change
    create_table :workoutequipments do |t|
      t.integer :workout_id, :null => false
      t.integer :equipment_id, :null => false

      t.timestamps
    end
  end
end
