class CreateExercisemusclegroups < ActiveRecord::Migration
  def change
    create_table :exercisemusclegroups do |t|
      t.integer :exercise_id
      t.integer :muscle_group_id

      t.timestamps
    end
  end
end
