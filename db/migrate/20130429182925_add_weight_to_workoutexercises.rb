class AddWeightToWorkoutexercises < ActiveRecord::Migration
  def change
    add_column :workoutexercises, :exercise_weight, :integer
  end
end
