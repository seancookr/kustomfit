class RemoveMuscleGroupFromExercises < ActiveRecord::Migration
  def up
    remove_column :exercises, :muscle_group
  end

  def down
    add_column :exercises, :muscle_group, :string
  end
end
