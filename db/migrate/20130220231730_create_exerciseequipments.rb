class CreateExerciseequipments < ActiveRecord::Migration
  def change
    create_table :exerciseequipments do |t|
      t.integer :exercise_id, :null => false
      t.integer :equipment_id, :null => false

      t.timestamps
    end
  end
end
