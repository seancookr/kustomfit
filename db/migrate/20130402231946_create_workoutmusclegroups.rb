class CreateWorkoutmusclegroups < ActiveRecord::Migration
  def change
    create_table :workoutmusclegroups do |t|
      t.integer :workout_id
      t.integer :muscle_group_id

      t.timestamps
    end
  end
end
