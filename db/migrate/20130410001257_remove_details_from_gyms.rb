class RemoveDetailsFromGyms < ActiveRecord::Migration
  def up
    remove_column :gyms, :zip
    remove_column :gyms, :phone_number
  end

  def down
    add_column :gyms, :phone_number, :integer
    add_column :gyms, :zip, :integer
  end
end
