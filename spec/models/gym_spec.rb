# == Schema Information
#
# Table name: gyms
#
#  id             :integer          not null, primary key
#  name           :string(255)
#  street_address :string(255)
#  city           :string(255)
#  state          :string(255)
#  website        :string(255)
#  hours          :string(255)
#  created_at     :datetime         not null
#  updated_at     :datetime         not null
#  latitude       :float
#  longitude      :float
#  phone_number   :string(255)
#  zip            :string(255)
#

require File.dirname(__FILE__) + '/../spec_helper'

describe Gym do
end
