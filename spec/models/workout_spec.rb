# == Schema Information
#
# Table name: workouts
#
#  id         :integer          not null, primary key
#  name       :string(255)
#  created_at :datetime         not null
#  updated_at :datetime         not null
#  user_id    :integer
#  category   :string(255)
#

require File.dirname(__FILE__) + '/../spec_helper'

describe Workout do
  before do
    @workout = Workout.new(name: "Example Workout", category: "circuit training")
  end
  
  subject { @workout }
  
  it { should respond_to(:name) }
  it { should respond_to(:category) }
end
