require 'spec_helper'

describe "equipment/new" do
  before(:each) do
    assign(:equipment, stub_model(Equipment,
      :name => "MyString"
    ).as_new_record)
  end

  it "renders new equipment form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form", :action => equipment_index_path, :method => "post" do
      assert_select "input#equipment_name", :name => "equipment[name]"
    end
  end
end
