class WorkoutsController < ApplicationController
  before_filter :admin_user,     only: :index
  before_filter :signed_in_user
  before_filter :correct_workout, only: [:show, :edit, :update, :destroy]
  
  def index
    @workouts = Workout.all
  end

  def show
    @workout = Workout.find(params[:id])
    @workout_equipment     = @workout.equipments
    @workout_exercises     = @workout.exercises
    @workout_muscle_groups = @workout.muscle_groups
    @workoutexercises      = @workout.workoutexercises
  end

  def new
    @workout = Workout.new
  end

  def create
    @workout = current_user.workouts.build(params[:workout])
    if @workout.save
      flash[:success] = "Workout created!" 
      @workout.add_exercises
      redirect_to @workout
    else
      render 'new'
    end
  end

  def edit
    @workout = Workout.find(params[:id])
  end

  def update
    @workout = Workout.find(params[:id])
    if @workout.update_attributes(params[:workout])
      redirect_to @workout, :notice  => "Successfully updated workout."
    else
      render :action => 'edit'
    end
  end

  def destroy
    @workout = Workout.find(params[:id])
    @workout.destroy
    redirect_to root_path, :notice => "Successfully destroyed workout."
  end
  
  private
  
    def correct_workout
      @workout = Workout.find(params[:id])
      redirect_to(root_path) unless current_user.id == @workout.user_id
    end
  
end
