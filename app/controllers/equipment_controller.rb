class EquipmentController < ApplicationController
  before_filter :admin_user

  def index
    @equipment = Equipment.all
  end

  def show
    @equipment = Equipment.find(params[:id])
    @exercises = @equipment.exercises.all
  end

  def new
    @equipment = Equipment.new
  end

  def edit
    @equipment = Equipment.find(params[:id])
  end

  def create
    @equipment = Equipment.new(params[:equipment])

    if @equipment.save
      flash[:success] = "Equipment was successfully created."
      @equipment = Equipment.all
      render "index"
    else
      format.html { render action: "new" }
      format.json { render json: @equipment.errors, status: :unprocessable_entity }
    end
  end

  # PUT /equipment/1.json
  def update
    @equipment = Equipment.find(params[:id])

    if @equipment.update_attributes(params[:equipment])
      flash[:success] = "Equipment was successfully updated."
      @equipment = Equipment.all
      render "index"
    else
      format.html { render action: "edit" }
      format.json { render json: @equipment.errors, status: :unprocessable_entity }
    end
  end

  # DELETE /equipment/1.json
  def destroy
    @equipment = Equipment.find(params[:id])
    @equipment.destroy

    respond_to do |format|
      format.html { redirect_to equipment_index_url }
      format.json { head :no_content }
    end
  end
end
