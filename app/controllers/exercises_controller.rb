class ExercisesController < ApplicationController
  before_filter :admin_user

  def index
    @exercises = Exercise.paginate(page: params[:page], :per_page => 10)
  end

  def show
    @exercise = Exercise.find(params[:id])
    @equipment = @exercise.equipments.all
  end

  def new
    @exercise = Exercise.new
  end

  def edit
    @exercise = Exercise.find(params[:id])       
  end

  def create
    @exercise = Exercise.new(params[:exercise])
    
    if @exercise.save
      flash[:success] = "Exercises was successfully created."
      @exercises = Exercise.paginate(page: params[:page], :per_page => 10)
      render "index"
    else
      format.html { render action: "new" }
      format.json { render json: @exercise.errors, status: :unprocessable_entity }
    end
  end

  def update
    @exercise = Exercise.find(params[:id])

    if @exercise.update_attributes(params[:exercise])
      flash[:success] = "Exercises was successfully updated."
       @exercises = Exercise.paginate(page: params[:page], :per_page => 10)
      render "index"
    else
      format.html { render action: "edit" }
      format.json { render json: @exercise.errors, status: :unprocessable_entity }
    end
  end

  def destroy
    @exercise = Exercise.find(params[:id])
    @exercise.destroy

    respond_to do |format|
      format.html { redirect_to exercises_url }
      format.json { head :no_content }
    end
  end
end
