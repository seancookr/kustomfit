class GymsController < ApplicationController
  helper_method :sort_column, :sort_direction
  
  def index
    @gyms = Gym.near([current_user.latitude, current_user.longitude], 10, :order => :distance).paginate(page: params[:page], :per_page => 10)
  end

  def show
    @gym = Gym.find(params[:id])
  end

  def new
    @gym = Gym.new
  end

  def create
    @gym = Gym.new(params[:gym])
    if @gym.save
      redirect_to @gym, :notice => "Successfully created gym."
    else
      render :action => 'new'
    end
  end

  def edit
    @gym = Gym.find(params[:id])
  end

  def update
    @gym = Gym.find(params[:id])
    if @gym.update_attributes(params[:gym])
      redirect_to @gym, :notice  => "Successfully updated gym."
    else
      render :action => 'edit'
    end
  end

  def destroy
    @gym = Gym.find(params[:id])
    @gym.destroy
    redirect_to gyms_url, :notice => "Successfully destroyed gym."
  end
  
  private
  
  def sort_column
    Gym.column_names.include?(params[:sort]) ? params[:sort] : "name"
  end
  
  def sort_direction
    %w[asc desc].include?(params[:direction]) ? params[:direction] : "asc"
  end
end
