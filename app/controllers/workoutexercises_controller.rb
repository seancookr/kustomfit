class WorkoutexercisesController < ApplicationController

  
  def update
    @workout_exercise = Workoutexercise.find(params[:id])
    if @workout_exercise.update_attributes(params[:workoutexercise])
      flash[:success] = "Weight Saved"
      respond_to do |format|
        format.html
        format.mobile
        format.js
      end
    end
  end
end
