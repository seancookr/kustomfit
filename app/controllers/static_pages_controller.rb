class StaticPagesController < ApplicationController
  
  def home
    @workouts = current_user.workouts.paginate(page: params[:page], :per_page => 10) if signed_in?
  end

  def help
  end
  
  def about
  end
end
