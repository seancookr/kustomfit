class NotificationsMailer < ActionMailer::Base

  default :from => "mail@kustomfit.org"
  default :to => "mail@kustomfit.org"

  def new_message(message)
    @message = message
    mail(:subject => "[Kustomfit] #{message.subject}")
  end

end