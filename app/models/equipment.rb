# == Schema Information
#
# Table name: equipment
#
#  id         :integer          not null, primary key
#  name       :string(255)      not null
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class Equipment < ActiveRecord::Base
  attr_accessible :name, :exercise_ids, :workout_ids, :gym_ids
  
  validates :name, :presence => true
  
  has_many    :exerciseequipments
  has_many    :exercises, :through => :exerciseequipments
  has_many    :workoutequipments
  has_many    :workouts,  :through => :workoutequipments
  has_many    :gymequipments
  has_many    :gyms,      :through => :gymequipments
end
