# == Schema Information
#
# Table name: exerciseequipments
#
#  id           :integer          not null, primary key
#  exercise_id  :integer          not null
#  equipment_id :integer          not null
#  created_at   :datetime         not null
#  updated_at   :datetime         not null
#

class Exerciseequipment < ActiveRecord::Base
  attr_accessible :equipment_id, :exercise_id
  
  belongs_to :exercise
  belongs_to :equipment
end
