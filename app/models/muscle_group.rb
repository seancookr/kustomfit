# == Schema Information
#
# Table name: muscle_groups
#
#  id         :integer          not null, primary key
#  name       :string(255)
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class MuscleGroup < ActiveRecord::Base
  attr_accessible :name
  
  has_many :workoutmusclegroups
  has_many :workouts, :through => :workoutmusclegroups
  has_many :exercisemusclegroups
  has_many :exercises, :through => :exercisemusclegroups
end
