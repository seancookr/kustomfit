class Workoutexercises < ActiveRecord::Base
  attr_accessible :exercise_id, :workout_id
  
  belongs_to :workout
  belongs_to :exercise
end
