# == Schema Information
#
# Table name: workoutequipments
#
#  id           :integer          not null, primary key
#  workout_id   :integer          not null
#  equipment_id :integer          not null
#  created_at   :datetime         not null
#  updated_at   :datetime         not null
#

class Workoutequipment < ActiveRecord::Base
  attr_accessible :equipment_id, :workout_id
  
  belongs_to :workout
  belongs_to :equipment
end
