# == Schema Information
#
# Table name: gyms
#
#  id             :integer          not null, primary key
#  name           :string(255)
#  street_address :string(255)
#  city           :string(255)
#  state          :string(255)
#  website        :string(255)
#  hours          :string(255)
#  created_at     :datetime         not null
#  updated_at     :datetime         not null
#  latitude       :float
#  longitude      :float
#  phone_number   :string(255)
#  zip            :string(255)
#

class Gym < ActiveRecord::Base
  attr_accessible :name, :street_address, :city, :state, :zip, :phone_number, :website, :hours, :equipment_ids, :latitude, :logintude
  geocoded_by :full_address 
  after_validation :geocode
  
  validates :name, presence: true
  validates :street_address, presence: true
  validates :city, presence: true
  validates :state, presence: true
  validates :zip, presence: true
  validates :phone_number, presence: true
  validates :website, presence: true
  
  has_many :gymequipments
  has_many :equipments, :through => :gymequipments
  
  def full_address
    [street_address, city, state].compact.join(', ')
  end
end
