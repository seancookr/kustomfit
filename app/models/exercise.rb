# == Schema Information
#
# Table name: exercises
#
#  id               :integer          not null, primary key
#  name             :string(255)      not null
#  created_at       :datetime         not null
#  updated_at       :datetime         not null
#  circuit_reps     :string(255)
#  strength_reps    :string(255)
#  exercise_type    :string(255)
#  hypertrophy_reps :string(255)
#

class Exercise < ActiveRecord::Base
  attr_accessible :name, :exercise_type, :equipment_ids, :workout_ids, :circuit_reps, 
                  :strength_reps, :hypertrophy_reps, :muscle_group_ids, :instructions
  
  validates       :name,             :presence => true,
                                     uniqueness: { case_sensitive: false }
  validates       :circuit_reps,     :presence => true
  validates       :strength_reps,    :presence => true
  validates       :hypertrophy_reps, :presence => true
                  
  has_many        :exerciseequipments
  has_many        :equipments,    :through => :exerciseequipments
  has_many        :workoutexercises
  has_many        :workouts,      :through => :workoutexercises
  has_many        :exercisemusclegroups
  has_many        :muscle_groups, :through => :exercisemusclegroups
end
