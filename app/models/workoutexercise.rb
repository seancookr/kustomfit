# == Schema Information
#
# Table name: workoutexercises
#
#  id              :integer          not null, primary key
#  workout_id      :integer          not null
#  exercise_id     :integer          not null
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#  position        :integer
#  exercise_weight :integer
#

class Workoutexercise < ActiveRecord::Base
  attr_accessible :exercise_id, :workout_id, :exercise_weight
  
  belongs_to :workout
  belongs_to :exercise
  
  acts_as_list :scope => :workout

end
