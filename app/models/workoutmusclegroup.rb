# == Schema Information
#
# Table name: workoutmusclegroups
#
#  id              :integer          not null, primary key
#  workout_id      :integer
#  muscle_group_id :integer
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#

class Workoutmusclegroup < ActiveRecord::Base
  attr_accessible :muscle_group_id, :workout_id
  
  belongs_to :workout
  belongs_to :muscle_group
end
