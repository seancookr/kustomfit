# == Schema Information
#
# Table name: gymequipments
#
#  id           :integer          not null, primary key
#  gym_id       :integer
#  equipment_id :integer
#  created_at   :datetime         not null
#  updated_at   :datetime         not null
#

class Gymequipment < ActiveRecord::Base
  attr_accessible :equipment_id, :gym_id
  
  belongs_to :gym
  belongs_to :equipment
end
