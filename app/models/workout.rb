# == Schema Information
#
# Table name: workouts
#
#  id         :integer          not null, primary key
#  name       :string(255)
#  created_at :datetime         not null
#  updated_at :datetime         not null
#  user_id    :integer
#  category   :string(255)
#

class Workout < ActiveRecord::Base
  attr_accessible :name, :category, :equipment_ids, :exercises_ids, :muscle_group_ids, :workoutexercises_attributes
  belongs_to  :user
  
  validates   :user_id, presence: true
  validates   :category, presence: true
  
  has_many    :workoutequipments
  has_many    :equipments, :through => :workoutequipments
  
  has_many    :workoutmusclegroups
  has_many    :muscle_groups, :through => :workoutmusclegroups
  
  has_many    :workoutexercises, order: :position
  has_many    :exercises, :through => :workoutexercises, order: :position
  accepts_nested_attributes_for :workoutexercises
  
  default_scope order: 'workouts.created_at DESC'
  
  def add_exercises
    exercise_list = self.arrange_exercises
    exercise_list.each do |exercise|
      self.workoutexercises.create(:exercise_id => exercise.id)
    end
  end
  
  def retrieve_exercises_from_equipment
    self.equipments.map(&:exercises).flatten
  end
      
  def retrieve_exercises_from_muscle_groups
    self.muscle_groups.map(&:exercises).flatten
  end
  
  def filter_exercises
    t = Exercise.arel_table
    exercise_list = self.available_exercises
    filter_keywords = ["Bench Press", "Squat", "Clean", "Deadlift",
                       "Raise", "Lunge", "Good Morning", "Row", "Fly", "Press"]
    filter_keywords.each do |keyword|
      filter_list = exercise_list & Exercise.where(t[:name].matches("%" + keyword))
      exercise_list = exercise_list - filter_list
      filter_list = filter_list.shuffle
      if filter_list.count >= 2
        exercise_list << filter_list[0, 1]
      else
        exercise_list << filter_list
      end
    end
    exercise_list.flatten
  end
  
  def available_exercises
    if self.muscle_groups.count >= 1
      exercise_list = self.retrieve_exercises_from_equipment & self.retrieve_exercises_from_muscle_groups
    else
      exercise_list = self.retrieve_exercises_from_equipment
    end
    exercise_list
  end
  
  def arrange_exercises
    exercise_list = self.filter_exercises
    compound_list = exercise_list & Exercise.where(:exercise_type => "Compound Lift")
    iso_list = exercise_list & Exercise.where(:exercise_type => "Isolation Lift")
    compound_list = compound_list.shuffle
    iso_list = iso_list.shuffle
    workout_list = []
    if (compound_list.count + iso_list.count) >= 12
      if self.category = "Strength Training" 
        workout_list = self.list_builder(compound_list, iso_list)
      elsif self.category = "Hypertrophy Training"
        workout_list = self.list_builder(compound_list, iso_list)
      else
        workout_list = self.list_builder(compound_list, iso_list)
      end
      workout_list = workout_list.flatten.shuffle[0..11]
    else
      workout_list = exercise_list
    end
    workout_list
  end
  
  def list_builder(comp_list, iso_list)
    n = comp_list.count
    m = iso_list.count
    workout_list = []
    unless workout_list.count >= 12 || workout_list.count == (comp_list.count + iso_list.count)
      while n > 0 || m > 0
        if comp_list[n-1]
          workout_list << comp_list[n-1]
        end
        if iso_list[n-1]
          workout_list << iso_list[n-1]
        end
        n = n - 1
        m = m - 1
      end
    end
    workout_list
  end
  
end
