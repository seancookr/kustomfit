# == Schema Information
#
# Table name: exercisemusclegroups
#
#  id              :integer          not null, primary key
#  exercise_id     :integer
#  muscle_group_id :integer
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#

class Exercisemusclegroup < ActiveRecord::Base
  attr_accessible :exercises_id, :muscle_group_id
  
  belongs_to :exercise
  belongs_to :muscle_group
end
